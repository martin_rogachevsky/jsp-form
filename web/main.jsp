<%--
  Created by IntelliJ IDEA.
  User: Martin Rogachevsky
  Date: 5/10/2018
  Time: 8:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Result</title>
</head>
<body>
<h1>Calculated words:</h1>

    <ul>
        <li><p><b>Line 1:</b>
            <%= request.getParameter("line1").split(" +").length%>
        </p></li>
        <li><p><b>line 2:</b>
            <%= request.getParameter("line2").split(" +").length%>
        </p></li>
        <li><p><b>Line 3:</b>
            <%= request.getParameter("line3").split(" +").length%>
        </p></li>
    </ul>
    <a href="index.jsp">Go to Index</a>
</body>
</html>
